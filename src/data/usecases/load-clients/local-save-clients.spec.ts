import { LocalLoadClients } from "@/data/usecases";
import { mockClients, CacheStoreSpy } from "@/data/tests";

type ModelTypes = {
  model: LocalLoadClients;
  cacheStore: CacheStoreSpy;
};

const makeModel = (timestamp = new Date()): ModelTypes => {
  const cacheStore = new CacheStoreSpy();
  const model = new LocalLoadClients(cacheStore, timestamp);
  return { cacheStore, model };
};

describe("LocalSaveClients", () => {
  test("Should not delete or insert cache on init", async () => {
    const { cacheStore } = makeModel();
    expect(cacheStore.messages).toEqual([]);
  });

  test("Should call delete with correct key", async () => {
    const { model, cacheStore } = makeModel();
    await model.save(mockClients());
    expect(cacheStore.messages).toEqual([
      CacheStoreSpy.Message.delete,
      CacheStoreSpy.Message.insert,
    ]);
    expect(cacheStore.deleteKey).toBe("clients");
  });

  test("Should not insert new Cache if delete fails", async () => {
    const { model, cacheStore } = makeModel();
    cacheStore.simulateDeleteError();
    const promise = model.save(mockClients());
    expect(cacheStore.messages).toEqual([CacheStoreSpy.Message.delete]);
    await expect(promise).rejects.toThrow();
  });

  test("Should insert new Cache if delete succeeds", async () => {
    const timestamp = new Date();
    const { model, cacheStore } = makeModel(timestamp);
    const clients = mockClients();
    const promise = model.save(clients);
    expect(cacheStore.messages).toEqual([
      CacheStoreSpy.Message.delete,
      CacheStoreSpy.Message.insert,
    ]);
    expect(cacheStore.insertKey).toBe("clients");
    expect(cacheStore.insertValues).toEqual({ timestamp, value: clients });
    await expect(promise).resolves.toBeFalsy();
  });

  test("Should throw if insert throws", async () => {
    const { model, cacheStore } = makeModel();
    cacheStore.simulateInsertError();
    const promise = model.save(mockClients());
    expect(cacheStore.messages).toEqual([
      CacheStoreSpy.Message.delete,
      CacheStoreSpy.Message.insert,
    ]);
    await expect(promise).rejects.toThrow();
  });
});
