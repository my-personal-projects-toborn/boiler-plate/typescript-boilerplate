import { LocalLoadClients } from "@/data/usecases";
import {
  CacheStoreSpy,
  mockClients,
  getCacheExirationDate,
} from "@/data/tests";

type ModelTypes = {
  model: LocalLoadClients;
  cacheStore: CacheStoreSpy;
};

const makeModel = (timestamp = new Date()): ModelTypes => {
  const cacheStore = new CacheStoreSpy();
  const model = new LocalLoadClients(cacheStore, timestamp);
  return { cacheStore, model };
};

describe("LocalLoadClients", () => {
  //Contexto diferente do mesmo componente
  test("Should not delete or insert cache on init", async () => {
    const { cacheStore } = makeModel();
    expect(cacheStore.messages).toEqual([]);
  });

  test("Should return empty list if load fails", async () => {
    const { model, cacheStore } = makeModel();
    cacheStore.simulateFetchError();
    const clients = await model.loadAll();
    expect(cacheStore.messages).toEqual([CacheStoreSpy.Message.fetch]);
    expect(clients).toEqual([]);
  });

  test("Should return list of clients if cache is valid", async () => {
    const currentDate = new Date();
    const timestamp = getCacheExirationDate(currentDate);
    timestamp.setSeconds(timestamp.getSeconds() + 1);
    const { model, cacheStore } = makeModel(currentDate);
    cacheStore.fetchResult = { timestamp, value: mockClients() };
    const clients = await model.loadAll();
    expect(cacheStore.messages).toEqual([CacheStoreSpy.Message.fetch]);
    expect(clients).toEqual(cacheStore.fetchResult.value);
    expect(cacheStore.fetchKey).toBe("clients");
  });

  test("Should return empty list of clients if cache is expired", async () => {
    const currentDate = new Date();
    const timestamp = getCacheExirationDate(currentDate);
    timestamp.setSeconds(timestamp.getSeconds() - 1);
    const { model, cacheStore } = makeModel(currentDate);
    cacheStore.fetchResult = { timestamp, value: mockClients() };
    const clients = await model.loadAll();
    expect(cacheStore.messages).toEqual([CacheStoreSpy.Message.fetch]);
    expect(clients).toEqual([]);
    expect(cacheStore.fetchKey).toBe("clients");
  });

  test("Should return empty list of clients if cache is expired", async () => {
    const currentDate = new Date();
    const timestamp = getCacheExirationDate(currentDate);
    const { model, cacheStore } = makeModel(currentDate);
    cacheStore.fetchResult = { timestamp, value: mockClients() };
    const clients = await model.loadAll();
    expect(cacheStore.messages).toEqual([CacheStoreSpy.Message.fetch]);
    expect(clients).toEqual([]);
    expect(cacheStore.fetchKey).toBe("clients");
  });

  test("Should return an empty list of clients if cache is empty", async () => {
    const currentDate = new Date();
    const timestamp = getCacheExirationDate(currentDate);
    timestamp.setSeconds(timestamp.getSeconds() + 1);
    const { model, cacheStore } = makeModel(currentDate);
    cacheStore.fetchResult = { timestamp, value: [] };
    const clients = await model.loadAll();
    expect(cacheStore.messages).toEqual([CacheStoreSpy.Message.fetch]);
    expect(clients).toEqual([]);
    expect(cacheStore.fetchKey).toBe("clients");
  });
});
