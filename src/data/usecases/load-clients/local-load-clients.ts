import { CachePolicy, CacheStore } from "@/data/protocols/cache";
import { SaveClients, LoadClients } from "@/domain/usecases";

export class LocalLoadClients implements SaveClients, LoadClients {
  private readonly key = "clients";
  constructor(
    private readonly cacheStore: CacheStore,
    private readonly currentDate: Date
  ) {}
  async save(clients: Array<SaveClients.Params>): Promise<void> {
    this.cacheStore.replace(this.key, {
      timestamp: this.currentDate,
      value: clients,
    });
  }
  async loadAll(): Promise<Array<LoadClients.Result>> {
    try {
      const cache = this.cacheStore.fetch(this.key);
      return CachePolicy.validate(cache.timestamp, this.currentDate)
        ? cache.value
        : [];
    } catch (error) {
      return [];
    }
  }

  validate(): void {
    try {
      const cache = this.cacheStore.fetch(this.key);
      if (!CachePolicy.validate(cache.timestamp, this.currentDate)) {
        throw new Error();
      }
    } catch (error) {
      this.cacheStore.delete(this.key);
    }
  }
}
