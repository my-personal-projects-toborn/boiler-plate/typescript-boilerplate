import { LocalLoadClients } from "@/data/usecases";
import { CacheStoreSpy, getCacheExirationDate } from "@/data/tests";

type ModelTypes = {
  model: LocalLoadClients;
  cacheStore: CacheStoreSpy;
};

const makeModel = (timestamp = new Date()): ModelTypes => {
  const cacheStore = new CacheStoreSpy();
  const model = new LocalLoadClients(cacheStore, timestamp);
  return { cacheStore, model };
};

describe("LocalLoadClients", () => {
  //Contexto diferente do mesmo componente
  test("Should not delete or insert cache on init", async () => {
    const { cacheStore } = makeModel();
    expect(cacheStore.messages).toEqual([]);
  });

  test("Should delete cache if load fails", async () => {
    const { model, cacheStore } = makeModel();
    cacheStore.simulateFetchError();
    model.validate();
    expect(cacheStore.messages).toEqual([
      CacheStoreSpy.Message.fetch,
      CacheStoreSpy.Message.delete,
    ]);
    expect(cacheStore.deleteKey).toBe("clients");
  });

  test("Should has no side effects if load succeds", () => {
    const currentDate = new Date();
    const timestamp = getCacheExirationDate(currentDate);
    timestamp.setSeconds(timestamp.getSeconds() + 1);
    const { model, cacheStore } = makeModel(currentDate);
    cacheStore.fetchResult = { timestamp };
    model.validate();
    expect(cacheStore.messages).toEqual([CacheStoreSpy.Message.fetch]);
    expect(cacheStore.fetchKey).toBe("clients");
  });

  test("Should delete cache if is expired", () => {
    const currentDate = new Date();
    const timestamp = getCacheExirationDate(currentDate);
    timestamp.setSeconds(timestamp.getSeconds() - 1);
    const { model, cacheStore } = makeModel(currentDate);
    cacheStore.fetchResult = { timestamp };
    model.validate();
    expect(cacheStore.messages).toEqual([
      CacheStoreSpy.Message.fetch,
      CacheStoreSpy.Message.delete,
    ]);
    expect(cacheStore.fetchKey).toBe("clients");
    expect(cacheStore.deleteKey).toBe("clients");
  });

  test("Should delete cache if is on expiration date", async () => {
    const currentDate = new Date();
    const timestamp = getCacheExirationDate(currentDate);
    const { model, cacheStore } = makeModel(currentDate);
    cacheStore.fetchResult = { timestamp };
    model.validate();
    expect(cacheStore.messages).toEqual([
      CacheStoreSpy.Message.fetch,
      CacheStoreSpy.Message.delete,
    ]);

    expect(cacheStore.fetchKey).toBe("clients");
    expect(cacheStore.deleteKey).toBe("clients");
  });
});
