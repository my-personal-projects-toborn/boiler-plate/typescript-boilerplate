import { SaveClients } from "@/domain/usecases";
import faker from "faker";

export const mockClients = (): Array<SaveClients.Params> => [
  {
    id: faker.datatype.uuid(),
    name: faker.name.firstName(),
    birthday: faker.date.recent(),
  },
  {
    id: faker.datatype.uuid(),
    name: faker.name.firstName(),
    birthday: faker.date.recent(),
  },
];
