export type ClientModel = {
  id: string;
  name: string;
  birthday: Date;
};
