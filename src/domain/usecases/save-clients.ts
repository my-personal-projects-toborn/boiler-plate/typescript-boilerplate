import { ClientModel } from "@/domain/models";

export interface SaveClients {
  save: (clients: Array<SaveClients.Params>) => Promise<void>;
}

export namespace SaveClients {
  export type Params = ClientModel;
}
