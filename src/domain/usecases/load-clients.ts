import { ClientModel } from "@/domain/models";

export interface LoadClients {
  loadAll: () => Promise<Array<LoadClients.Result>>;
}

export namespace LoadClients {
  export type Result = ClientModel;
}
