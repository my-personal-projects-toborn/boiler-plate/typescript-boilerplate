module.exports = {
  roots: ["<rootDir>"],
  testEnvironment: "node",
  transform: { ".+\\.ts$": "ts-jest" },
  moduleNameMapper: { "@/(.*)": "<rootDir>/src/$1" },
};
